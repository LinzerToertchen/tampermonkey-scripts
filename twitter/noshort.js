// ==UserScript==
// @name         Twitter noShort
// @description  Skips twitters url-shortener so the real url is shown in your browser's status bar
// @version      0.1
// @author       Marian Hähnlein <marian@haehnlein.at>
// @namespace    https://gitlab.com/HaehnleinMar/tampermonkey-scripts
// @supportURL   https://gitlab.com/HaehnleinMar/tampermonkey-scripts/issues
// @match        https://twitter.com/*
// @grant        none
// @license      MIT
// ==/UserScript==


(function() {
    'use strict';

    document.body.addEventListener('DOMSubtreeModified', function () {
        var links = document.getElementsByClassName("twitter-timeline-link");

        for (let link of links) {
            link.href = !!link.dataset.expandedUrl ? link.dataset.expandedUrl : link.href;
        };
    }, false);
})();
